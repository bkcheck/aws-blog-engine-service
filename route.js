const assert = require('assert')
const util = require('util')
const routes = require('./routes')
const modules = require('./modules')
const ApiError = require('./lib/ApiError')

function getHeaders () {
  return {
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
    'Access-Control-Allow-Origin': 'http://localhost:3000',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Vary': 'Origin'
  }
}

async function routeRequest (event, context, callback) {
  assert(event, 'event is required')
  assert(context, 'context is required')
  assert(callback, 'callback is required')

  const params = event.queryStringParameters || {}
  const action = event.pathParameters.action
  let interceptToken = false

  params.id = event.pathParameters.id || null

  try {
    const route = getRoute()

    if (route.authenticates) {
      interceptToken = true
    }

    // authIfProtected returns info about the user. Not currently doing anything with that.
    await authIfProtected(route)

    const result = await executeRoute(route)

    respondSuccess(result)
  } catch (e) {
    respondFail(e)
  }

  function authIfProtected (route) {
    if (!route.auth) {
      return
    }

    let cookies = event.headers.Cookie
    let tokenRegex = /slsblog=([a-zA-Z0-9_\-\.\+\/=]+)/g // eslint-disable-line no-useless-escape

    let match = tokenRegex.exec(cookies)
    if (!match) {
      throw new ApiError(401, 'Unauthorized')
    }

    return decodeJwt(match[1])
  }

  function decodeJwt (token) {
    return new Promise(function (resolve, reject) {
      modules.jwt.verify(token, modules.jwtSecret, (err, decoded) => {
        if (err) {
          reject(err)
        } else {
          resolve(decoded)
        }
      })
    })
  }

  function executeRoute (route) {
    if (route.action === 'noop') {
      return
    }

    const fn = modules[route.action]

    if (!fn) {
      throw new ApiError()
    }

    let fnParams = {}
    if (route.getData) {
      fnParams = route.getData(JSON.parse(event.body), params)
    }

    return fn(fnParams, modules)
  }

  function getRoute () {
    if (action === null) {
      throw new ApiError(400, 'No action provided.')
    }

    const method = event.requestContext.httpMethod
    const routeName = util.format('%s_%s', (method === 'OPTIONS' ? '*' : action), method)
    const route = routes[routeName]

    if (!route) {
      throw new ApiError(400, 'Invalid method/action: ' + routeName)
    }

    return route
  }

  function respondFail (err) {
    const message = err.message || 'An error occurred while processing this request.'

    console.error(err)

    const response = {
      isBase64Encoded: false,
      statusCode: err.responseCode || 500,
      headers: getHeaders(),
      body: JSON.stringify({ message })
    }

    callback(null, response)
  }

  function respondSuccess (data) {
    let headers = getHeaders()

    if (interceptToken) {
      headers['Set-Cookie'] = util.format('slsblog=%s; path=/; max-age=10800', data)
      data = {}
    }

    const response = {
      isBase64Encoded: false,
      statusCode: 200,
      headers,
      body: JSON.stringify(data)
    }

    callback(null, response)
  }
}

module.exports = routeRequest

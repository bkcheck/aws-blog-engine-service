# Blog Engine Service
### A serverless blog engine hosted on AWS using API Gateway, Lambda, and DynamoDB.

##### Overview
*This is a work in progress!* I have not tested the deployment process on any AWS account
other than my own. A loose overview of what is needed to make this work:

1. AWS credentials correctly configured on a local machine for serverless access.
   An overview of which permissions are needed can be found in serverless.yml
2. A DynamoDB table properly configured (see below)
3. API Gateway properly configured to allow pass-through access to Lambda.

##### DynamoDB Table Structure
As of right now, this platform only requires use of one table named 'blog'. I'd like to create
an auto-deploy script in the future, but for now, this table will have to be manually created.

- Table Name: `blog`
- Partition Key: `type`
- Sort Key: `id`
- Indexes:
  - `slug-index`
    - Type: GSI
    - Partition Key: `slug`
    - Sort Key: (None)
    - Include projected attributes: `content`, `createdOn`, `modifiedOn`, `title`
  - `email-index`
    - Type: GSI
    - Partition Key: `email`
    - Sort Key: (None)
    - Include projected attributes: `salt`, `password`
  - `type-createdOn-index`
    - Type: LSI
    - Partition Key: `type`
    - Sort Key: `createdOn`
    - Projected Attributes: ALL

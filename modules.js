const aws = require('aws-sdk')
const deleteOne = require('./lib/db/deleteOne')
const getOne = require('./lib/db/getOne')
const putOne = require('./lib/db/putOne')
const config = require('./config')

aws.config.update({ region: 'us-west-1' })

module.exports = {
  authenticate: require('./lib/auth/authenticate'),
  createUser: require('./lib/auth/createUser'),
  crypto: require('crypto'),
  deletePost: deleteOne('post'),
  dynamo: new aws.DynamoDB.DocumentClient(),
  getPost: getOne('post'),
  getPostById: require('./lib/posts/getPostById'),
  getPostBySlug: require('./lib/posts/getPostBySlug'),
  getRecentPosts: require('./lib/posts/getRecentPosts'),
  getRecentPublicPosts: require('./lib/posts/getRecentPublicPosts'),
  getUser: getOne('user'),
  getUserByEmail: require('./lib/auth/getUserByEmail'),
  hashPassword: require('./lib/auth/hashPassword'),
  jwt: require('jsonwebtoken'),
  jwtSecret: config.jwtSecret,
  login: require('./lib/auth/login'),
  putPost: putOne('post'),
  putUser: putOne('user'),
  savePost: require('./lib/posts/savePost'),
  uuid: require('uuid/v4')
}

const assert = require('assert')

async function getRecentPublicPosts (params, deps) {
  assert(params, 'params')
  assert(deps, 'deps')
  assert(deps.getRecentPosts, 'deps.getRecentPosts')

  const posts = await deps.getRecentPosts(params, deps)

  posts.forEach(post => { delete post.id })

  return posts
}

module.exports = getRecentPublicPosts

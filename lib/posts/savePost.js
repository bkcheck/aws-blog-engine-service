const assert = require('assert')
const ApiError = require('../ApiError')

async function savePost (params, deps) {
  assert(params, 'params')
  assert(params.post, 'params.post')
  assert(deps, 'deps')
  assert(deps.getPostBySlug, 'deps.getPostBySlug')
  assert(deps.putPost, 'deps.putPost')

  const { post } = params

  validate()
  checkForSlugUsage()
  setDefaults()

  return deps.putPost(post, deps)

  async function checkForSlugUsage () {
    const existing = await deps.getPostBySlug({ slug: post.slug }, deps)
    if (existing) {
      throw new ApiError(401, 'A post with the slug ' + post.slug + ' already exists.')
    }
  }

  function setDefaults () {
    post.createdOn = params.post.createdOn || (new Date().toISOString())
    post.modifiedOn = new Date().toISOString()
  }

  function validate () {
    assert(post.title, 'post.title is required')
    assert(post.content, 'post.content is required')
    assert(post.slug, 'post.slug is required')
  }
}

module.exports = savePost

const assert = require('assert')

async function getPostBySlug (params, deps) {
  assert(params, 'params')
  assert(params.slug, 'params.slug')
  assert(deps, 'deps')
  assert(deps.dynamo, 'deps.dynamo')

  const { Items } = await query()
  return Items && Items[0]

  function query () {
    const opts = {
      TableName: 'blog',
      IndexName: 'slug-index',
      KeyConditionExpression: 'slug = :slug',
      ExpressionAttributeValues: { ':slug': params.slug }
    }
    return deps.dynamo.query(opts).promise()
  }
}

module.exports = getPostBySlug

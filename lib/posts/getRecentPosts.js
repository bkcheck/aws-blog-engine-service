const assert = require('assert')
const util = require('util')

async function getRecentPosts (params, deps) {
  assert(params, 'params')
  assert(deps, 'deps')
  assert(deps.dynamo, 'deps.query')

  const opts = getQuery()
  const { Items } = await deps.dynamo.query(opts).promise()

  return sort()

  function getQuery () {
    let ExpressionAttributeValues = { ':type': 'post' }
    let KeyConditionExpression = '#t = :type'
    let ScanIndexForward = false
    let comparisonOperator = '<'

    if (params.asc) {
      ScanIndexForward = true
      comparisonOperator = '>'
    }

    if (params.start) {
      ExpressionAttributeValues[':createdOn'] = params.start
      KeyConditionExpression += util.format(' AND createdOn %s :createdOn', comparisonOperator)
    }

    return {
      TableName: 'blog',
      IndexName: 'type-createdOn-index',
      KeyConditionExpression,
      ExpressionAttributeNames: { '#t': 'type' },
      ExpressionAttributeValues,
      ScanIndexForward,
      Limit: params.limit || 10
    }
  }

  function sort () {
    // If params.asc was specified, we still want to return posts with the most
    // recent first, so we need to reverse them.
    if (params.asc) {
      return Items.sort((a, b) => {
        return new Date(b.createdOn) - new Date(a.createdOn)
      })
    }
    return Items
  }
}

module.exports = getRecentPosts

const assert = require('assert')
const ApiError = require('../ApiError')

async function getPostById (params, deps) {
  assert(params, 'params')
  assert(params.id, 'params.id')
  assert(deps, 'deps')
  assert(deps.getPost, 'deps.getPost')

  const { Item } = await deps.getPost(params.id, deps)

  if (!Item) {
    throw new ApiError(404, 'Post not found: ' + params.id)
  }

  return Item
}

module.exports = getPostById

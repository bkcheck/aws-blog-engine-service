const assert = require('assert')

function putOne (type) {
  assert(type, 'putOne: type is required')

  return function (obj, deps) {
    return new Promise(function (resolve, reject) {
      assert(obj, 'obj')
      assert(deps, 'deps')
      assert(deps.dynamo, 'deps.dynamo')
      assert(deps.uuid, 'deps.uuid')

      if (!obj.id) {
        Object.assign(obj, { id: deps.uuid() })
      }

      const opts = {
        TableName: 'blog',
        Item: Object.assign(obj, { type })
      }

      deps.dynamo.put(opts, (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
  }
}

module.exports = putOne

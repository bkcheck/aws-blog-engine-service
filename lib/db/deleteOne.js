const assert = require('assert')

function deleteOne (type) {
  assert(type, 'deleteOne: type is required')

  return function (id, deps) {
    assert(id, 'deleteOne: id is required')
    assert(deps, 'deleteOne: deps is required')
    assert(deps.dynamo, 'deleteOne: deps.dynamo is required')

    const opts = {
      TableName: 'blog',
      Key: { type, id }
    }

    return deps.dynamo.delete(opts).promise()
  }
}

module.exports = deleteOne

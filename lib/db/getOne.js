const assert = require('assert')

function getOne (type) {
  assert(type, 'getOne: type is required')

  return async function (id, deps) {
    assert(id, 'getOne: id is required')
    assert(deps, 'getOne: deps is required')
    assert(deps.dynamo, 'getOne: deps.dynamo is required')

    const opts = {
      TableName: 'blog',
      Key: { type, id }
    }

    let response = await deps.dynamo.get(opts).promise()

    if (response.Item) {
      delete response.Item.type
    }

    return response
  }
}

module.exports = getOne

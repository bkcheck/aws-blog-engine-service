class ApiError extends Error {
  constructor (responseCode, message) {
    super(message)
    this.message = message
    this.responseCode = responseCode
  }
}

module.exports = ApiError

const assert = require('assert')

async function login (params, deps) {
  assert(params, 'params')
  assert(params.email, 'params.email')
  assert(params.password, 'params.password')
  assert(deps, 'deps')
  assert(deps.authenticate, 'deps.authenticate')
  assert(deps.jwt, 'deps.jwt')
  assert(deps.jwtSecret, 'deps.jwtSecret')

  const user = await deps.authenticate(params, deps)
  return getJwt(user)

  function getJwt (user) {
    return new Promise(function (resolve, reject) {
      let data = {
        name: user.name,
        email: user.email
      }
      deps.jwt.sign(data, deps.jwtSecret, { expiresIn: '3h' }, (err, token) => {
        if (err) {
          reject(err)
        } else {
          resolve(token)
        }
      })
    })
  }
}

module.exports = login

const assert = require('assert')
const ApiError = require('../ApiError')

async function createUser (params, deps) {
  assert(params, 'params')
  assert(params.user, 'params.user')
  assert(params.user.email, 'params.user.email')
  assert(params.user.name, 'params.user.name')
  assert(params.user.password, 'params.user.password')
  assert(deps, 'deps')
  assert(deps.crypto, 'deps.crypto')
  assert(deps.getUserByEmail, 'deps.getUserByEmail')
  assert(deps.hashPassword, 'deps.hashPassword')
  assert(deps.putUser, 'deps.putUser')

  await checkForExistingUser()
  const salt = await generateSalt()
  await hashPassword()

  return deps.putUser(params.user)

  async function checkForExistingUser () {
    const user = await deps.getUserByEmail({ email: params.user.email }, deps)
    if (user) {
      throw new ApiError(400, 'A user with this email already exists.')
    }
  }

  function generateSalt () {
    return new Promise(function (resolve, reject) {
      if (params.salt) {
        resolve(params.salt)
      } else {
        deps.crypto.randomBytes(8, (err, buf) => {
          if (err) {
            reject(err)
          } else {
            resolve(buf.toString('base64'))
          }
        })
      }
    })
  }

  async function hashPassword () {
    const hashed = await deps.hashPassword({ password: params.user.password, salt }, deps)
    params.user.password = hashed
    params.user.salt = salt
  }
}

module.exports = createUser

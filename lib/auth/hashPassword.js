const assert = require('assert')

async function hashPassword (params, deps) {
  assert(params, 'params')
  assert(params.password, 'params.password')
  assert(deps, 'deps')
  assert(deps.crypto, 'deps.crypto')

  const salt = await getSalt()
  return hash(salt)

  function hash (salt) {
    return new Promise(function (resolve, reject) {
      deps.crypto.pbkdf2(params.password, salt, 20000, 64, 'sha512', (err, derivedKey) => {
        if (err) {
          reject(err)
        } else {
          resolve(derivedKey.toString('base64'))
        }
      })
    })
  }

  function getSalt () {
    return new Promise(function (resolve, reject) {
      if (params.salt) {
        resolve(params.salt)
      } else {
        deps.crypto.randomBytes(8, (err, buf) => {
          if (err) {
            reject(err)
          } else {
            resolve(buf.toString('base64'))
          }
        })
      }
    })
  }
}

module.exports = hashPassword

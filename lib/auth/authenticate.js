const assert = require('assert')
const ApiError = require('../ApiError')

async function authenticate (params, deps) {
  assert(params, 'params')
  assert(params.email, 'params.email')
  assert(params.password, 'params.password')
  assert(deps, 'deps')
  assert(deps.hashPassword, 'deps.hashPassword')
  assert(deps.getUserByEmail, 'deps.getUserByEmail')

  const { email, password } = params
  const user = await deps.getUserByEmail({ email }, deps)

  return hashAndCompare

  async function hashAndCompare () {
    if (!user) {
      throw new ApiError(403, 'Invalid email/password.')
    } else {
      const hashed = await deps.hashPassword({ password, salt: user.salt }, deps)
      if (hashed === user.password) {
        return user
      } else {
        throw new ApiError(401, 'Invalid email/password.')
      }
    }
  }
}

module.exports = authenticate

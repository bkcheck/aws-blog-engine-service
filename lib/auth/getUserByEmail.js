const assert = require('assert')

async function getUserByEmail (params, deps) {
  assert(params, 'params')
  assert(params.email, 'params.email')
  assert(deps, 'deps')
  assert(deps.dynamo, 'deps.dynamo')

  const { Items } = await query()
  return Items[0]

  function query () {
    return new Promise(function (resolve, reject) {
      const opts = {
        TableName: 'blog',
        IndexName: 'email-index',
        KeyConditionExpression: 'email = :email',
        ExpressionAttributeValues: { ':email': params.email }
      }

      return deps.dynamo.query(opts, (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
  }
}

module.exports = getUserByEmail

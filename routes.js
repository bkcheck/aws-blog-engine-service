module.exports = {
  'login_POST': {
    action: 'login',
    authenticates: true,
    getData: body => {
      return body
    }
  },
  'post_DELETE': {
    action: 'deletePost',
    auth: true,
    getData: (body, params) => {
      return params.id
    }
  },
  'post_GET': {
    action: 'getPostById',
    auth: true,
    getData: (body, params) => {
      return { id: params.id }
    }
  },
  'post_PUT': {
    action: 'savePost',
    auth: true,
    getData: (body, params) => {
      return { post: body }
    }
  },
  'postBySlug_GET': {
    action: 'getPostBySlug',
    getData: (body, params) => {
      return { slug: params.id }
    }
  },
  'posts_GET': {
    action: 'getRecentPosts',
    auth: true,
    getData: (body, params) => {
      return { start: params.start, limit: params.limit, asc: params.asc }
    }
  },
  'recentPosts_GET': {
    action: 'getRecentPublicPosts',
    getData: (body, params) => {
      return { start: params.start, limit: params.limit, asc: params.asc }
    }
  },
  '*_OPTIONS': {
    action: 'noop'
  }
}

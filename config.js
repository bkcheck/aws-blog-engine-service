module.exports = {
  defaultTopPostCount: 5,
  jwtSecret: require('./jwtSecret'),
  localUrl: 'localhost:3000',
  mode: 'dev',
  postSegment: 10
}
